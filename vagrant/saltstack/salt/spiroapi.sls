python-pip: pkg.installed
python3-pip: pkg.installed

{% if grains['pythonversion'][0] == 2 %}
{% set pipbin = "/usr/bin/pip" %}
{% else %}
{% set pipbin = "/usr/bin/pip3" %}
{% endif %}


spiroapi:
  pip.installed:
    - bin_env: {{pipbin}}
    - editable: /opt/spiroapi/server

sshd:
  service.running

/etc/ssh/sshd_config:
  file.append:
    - text:
      - Subsystem spiroapi /usr/local/bin/spiroapi-ssh-subsystem
    - require:
      - pip: spiroapi
    - watch_in:
      - service: sshd

salt-master:
  service.running

/etc/salt/master.d/api.conf:
  file.serialize:
    - watch_in:
      - service: salt-master
    - formatter: yaml
    - dataset:
        publisher_acl:
          vagrant:
            - .*

/var/cache/salt:
  file.directory:
    - mode: 755

/var/cache/salt/master:
  file.directory:
    - mode: 755

/var/cache/salt/master/jobs:
  file.directory:
    - mode: 755

/var/run/salt:
  file.directory:
    - mode: 755

/var/run/salt/master:
  file.directory:
    - mode: 755
