"""
The low-level machinery to establish a msgpack pipe over SSH.

You probably just want MsgPackConnection
"""

import asyncio
import sys
import msgpack
import logging
from pathlib import Path

__all__ = 'MsgPackConnection',

log = logging.getLogger(__name__)


class BackpressureManager:
    """
    Handles backpressure and gating access to a callable.

    Raises a BrokenPipeError if called while shutdown.

    NOTE: If a coroutine is passed, it will have to be double-awaited.
    """

    def __init__(self, func):
        self._func = func
        self._is_blocked = asyncio.Event()
        self._is_open = True

        # Put us in a known state
        self.continue_calls()

    def pause_calls(self):
        """
        Pause calling temporarily.

        Does nothing if closed.
        """
        if self._is_open:
            self._is_blocked.clear()

    def continue_calls(self):
        """
        Continue calling.

        Does nothing if closed.
        """
        if self._is_open:
            self._is_blocked.set()

    def shutdown(self):
        """
        Causes calls to error.
        """
        self._is_open = False
        self._is_blocked.clear()

    async def __call__(self, *pargs, **kwargs):
        await self._is_blocked.wait()
        if self._is_open:
            return self._func(*pargs, **kwargs)
        else:
            raise BrokenPipeError


class MsgPackProcProtocol(asyncio.SubprocessProtocol):
    def __init__(self, recv_callback, error_stream=None):
        self._packer = msgpack.Packer(autoreset=True)
        self._unpacker = msgpack.Unpacker(raw=False)
        self.closed = asyncio.Future()
        self.error_stream = error_stream or sys.stderr.buffer
        self._write_proxy = BackpressureManager(self._real_write)
        self._write_proxy.pause_calls()

        self._recv_callback = recv_callback

    def connection_made(self, transport):
        log.debug("connection_made: %r", transport)
        self._transport = transport
        self._write_transport = self._transport.get_pipe_transport(0)
        self._write_proxy.continue_calls()

    def connection_lost(self, exc):
        log.debug("connection_lost: %r", exc)
        self._write_proxy.shutdown()

    def pause_writing(self):
        log.debug("pause_writing")
        self._write_proxy.pause_calls()

    def resume_writing(self):
        log.debug("resume_writing")
        self._write_proxy.continue_calls()

    def pipe_connection_lost(self, fd, exc):
        log.debug("pipe_connection_lost: %r %r", fd, exc)
        if fd == 0:  # SSH's stdin
            self._write_proxy.shutdown()

    def pipe_data_received(self, fd, data):
        log.debug("pipe_data_received: %r %r", fd, data)
        if fd == 1:  # SSH's stdout
            self._unpacker.feed(data)
            for msg in self._unpacker:
                log.debug("\t%r", msg)
                # PY37: asyncio.create_task()
                asyncio.ensure_future(self._recv_callback(msg))
        elif fd == 2:  # SSH's stderr
            self.error_stream.write(data)
            # Apply line flushing
            if b'\n' in data:
                self.error_stream.flush()

    def process_exited(self):
        log.debug("process_exited")
        self._write_proxy.shutdown()
        self.closed.set_result(True)
        # TODO: Flush the unpacker?

    async def send_message(self, message):
        """
        Send a message. May block due to backpressure.

        Raises BrokenPipeError if unable to send due to closed connection.
        """
        log.debug("send_message: %r", message)
        data = self._packer.pack(message)
        await self._write_proxy(data)

    def _real_write(self, data):
        log.debug("\tSending!")
        self._write_transport.write(data)


class MsgPackConnection:
    """
    Base class for msgpack connections, such as:

        class SpamSystem(MsgPackConnection):
            subsystem = 'spam'
            async def recv_message(self, message):
                print(message)

    Note: Do not just construct objects (like SpamSystem()). You must use the
    .establish_connection() factory, like:

    >>> await SpamSystem.establish_connection('foo.bar')
    """
    subsystem = None

    @classmethod
    async def establish_connection(cls, host, *, port=None, opts=None):
        """
        Connects to the given host (or user@host) and returns the
        MsgPackConnection.

        Only compatible with OpenSSH.
        """
        loop = asyncio.get_event_loop()

        self = cls()

        cmd = [
            'ssh', host, 
            # '-q',  # Quiet
            '-T',  # Disable psuedotty allocation
        ]

        if port:
            cmd += ['-p', str(port)]

        if opts:
            cmd += opts

        # Must be last
        cmd += ['-s', cls.subsystem]

        transport, protocol = await loop.subprocess_exec(
            lambda: MsgPackProcProtocol(
                self.recv_message,
                error_stream=cls.get_error_stream(),
            ),
            *cmd,
            stdin=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )

        self._protocol = protocol
        self._transport = transport

        return self

    @property
    def closed(self):
        """
        An asyncio.Future indicating if the connection has been closed.

        Note: "closed" means "the connection is completely destroyed". There are
        potential edge cases where the connection is half-open; closed will be
        unresolved (blocked, not fired) in that case.
        """
        return self._protocol.closed

    @classmethod
    def get_error_stream(self):
        """
        Return or construct the stream that the message log should be written to.

        Must return a non-blocking writable bytes stream. (See the io package.)

        If None is returned, stderr is used.

        Override me.
        """
        return None

    async def send_message(self, message):
        """
        Send a message.

        May block due to backpressure.

        Raises BrokenPipeError if the connection is closed.
        """
        await self._protocol.send_message(message)

    async def recv_message(self, message):
        """
        Called when a message has been received. Does not block the read loop
        and must be reentrant.

        Override me.
        """
        pass

    async def close(self):
        """
        Call to close and tear down the connection.
        """
        self._transport.terminate()
        # Allow pipes to be cleaned up manually
        await self.closed

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc, tb):
        await self.close()
