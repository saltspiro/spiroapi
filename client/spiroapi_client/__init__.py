# -*- coding: utf-8 -*-

"""Top-level package for SpiroAPI Client."""

__author__ = """Jamie Bliss"""
__email__ = 'jamie@ivyleav.es'
__version__ = '0.0.1'


import enum
import contextlib
from .sshmsgpack import MsgPackConnection
import asyncio
import logging
from .minioncall import PendingRequest

__all__ = 'SpiroAPIConnection', 'MinionError'

log = logging.getLogger(__name__)


class MsgID(enum.IntEnum):
    ShooshRequest = 0  # (C2S): request id

    StartMinionJob = 10  # (C2S): request id, target, target_type, function, pargs, kwargs
    MinionJobStarted = 11  # (S2C): request id, jid, speculated minions
    MinionJobResult = 12  # (S2C): request id, minion, return code, data
    MinionJobError = 14  # (S2C): request id, minion, message
    
    StartRunnerJob = 20  # (C2S): request id, function, pargs, kwargs
    RunnerJobStarted = 21  # (S2C): request id, jid
    RunnerJobComplete = 22  # (S2C): request id, data
    
    StartWheelJob = 30  # (C2S): request id, function, pargs, kwargs
    WheelJobStarted = 31  # (S2C): request id, jid
    WheelJobComplete = 32  # (S2C): request id, data
    
    EventSubscribe = 40  # (C2S): request id, pattern
    EventFired = 41  # (S2C): request id, tag, data


class IdManager(dict):
    @contextlib.contextmanager
    def generate(self, factory):
        if self:
            reqid = max(self.keys()) + 1
        else:
            reqid = 0

        self[reqid] = factory()
        try:
            yield reqid, self[reqid]
        finally:
            del self[reqid]


class MinionError(Exception):
    """
    Raised when a Minion throws an error.

    Has an attribute minion indicating what minion this came from.
    """
    minion: str
    def __init__(self, *args, minion):
        super().__init__(*args)
        self.minion = minion


class SpiroAPIConnection(MsgPackConnection):
    subsystem = 'spiroapi'

    def __init__(self):
        super().__init__()

        self.id_manager = IdManager()

    async def recv_message(self, message):
        if isinstance(message, str):
            # Received a textual log message
            print(message, file=sys.stderr)
        else:
            # Received an actual message
            try:
                mid = MsgID(message[0])
                reqid = message[1]
            except IndexError:
                # FIXME: Invalid message
                pass
            else:
                if reqid in self.id_manager:
                    await self.id_manager[reqid].put((mid, reqid, message[2:]))
                else:
                    log.warning("Received invalid request ID: %s", reqid)

    async def _channel(self, initmsg_id, *initmsg_args, always_shoosh=False):
        with self.id_manager.generate(asyncio.Queue) as (reqid, queue):
            await self.send_message([initmsg_id, reqid, *initmsg_args])
            try:
                while True:
                    msgid, rid, args = await queue.get()
                    if rid != reqid:
                        log.warning("Got a message for the wrong request")
                        continue
                    yield msgid, reqid, args
            except GeneratorExit:  # Regular exit
                if always_shoosh:
                    await self.send_message([MsgID.ShooshRequest, reqid])
            except:
                await self.send_message([MsgID.ShooshRequest, reqid])
                raise

    async def _minion_task(self, target_type, target, func, pargs, kwargs, request):
        async for msgid, reqid, args in self._channel(MsgID.StartMinionJob, target, target_type, func, pargs, kwargs):
            mid = None
            if msgid == MsgID.MinionJobStarted:
                jid, speculated_minions = args
                await request._job_started(jid, set(speculated_minions))
            elif msgid == MsgID.MinionJobResult:
                mid, retcode, data = args
                await request._got_result(mid, retcode, data)
            elif msgid == MsgID.MinionJobError:
                mid, msg = args
                # Since this is a task, I'm assuming that the callstack is
                # is unhelpful at this point.
                await request._got_error(mid, MinionError(msg, minion=mid))


    def minion(self, target_type: str, target: str, func: str, pargs, kwargs):
        """
        Run a minion job:
        * target_type, target: The minions to run the job against.
        * func: The name of the execution function to run
        * pargs, kwargs: The arguments to call the function with
        """
        return PendingRequest(
            lambda req: self._minion_task(target_type, target, func, pargs, kwargs, req)
        )


    async def events(self, glob: str):
        """
        Subscribe to the event bus, filtering by glob.

        * glob: A posix-like glob to filter events (must match the entire event
          tag)

        Generates 2-tuples:
        0. tag: The tag of the event
        1. data: The data body of the event

        If an event matches multiple subscriptions, it will be produced in each
        one.

        The subscription will be cancelled when this generator is killed (by
        exiting the consuming for loop, by cancelling the task, or any other
        means).
        """
        async for msgid, reqid, args in self._channel(MsgID.EventSubscribe, glob, always_shoosh=True):
            if msgid == MsgID.EventFired:
                tag, data = args
                yield tag, data
