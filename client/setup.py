#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

setup(
    name='spiroapi_client',
    version='0.0.1',
    author="Jamie Bliss",
    author_email='jamie@ivyleav.es',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="An async-first RPC system for Salt",
    install_requires=[
        'msgpack',
    ],
    long_description=readme,
    include_package_data=True,
    keywords='salt saltstack spiroapi',
    packages=find_packages(include=['spiroapi_client']),
    setup_requires=[],
    test_suite='tests',
    tests_require=[],
    url='https://spirostack.com/spiroapi/',
    zip_safe=True,
)
