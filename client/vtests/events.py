import asyncio
from pathlib import Path
import logging
from spiroapi_client import SpiroAPIConnection

logging.basicConfig(level=logging.INFO)


async def main():
    async with await SpiroAPIConnection.establish_connection(
        'vagrant@localhost',
        port=2222,
        opts=[
            '-i', str(
                Path(__file__).absolute().parent.parent.parent / 'vagrant' /
                '.vagrant' / 'machines' / 'spiroapi' / 'virtualbox' /
                'private_key'
            ),
        ]
    ) as conn:

        async for tag, data in conn.events('*'):
            print(tag, data)


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
