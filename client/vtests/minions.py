import asyncio
from pathlib import Path
import logging
from spiroapi_client import SpiroAPIConnection

logging.basicConfig(level=logging.INFO)


async def main():
    async with await SpiroAPIConnection.establish_connection(
        'vagrant@localhost',
        port=2222,
        opts=[
            '-i', str(
                Path(__file__).absolute().parent.parent.parent / 'vagrant' /
                '.vagrant' / 'machines' / 'spiroapi' / 'virtualbox' /
                'private_key'
            ),
        ]
    ) as conn:

        async for mid, res, data in conn.minion('glob', '*', 'test.ping', (), {}):
            print(mid, res, data)

        async for mid, res, data in conn.minion('glob', '*', 'test.arg_clean', (), {'spam': 'eggs'}):
            print(mid, res, data)

        async for mid, res, data in conn.minion('glob', '*', 'test.exception', ('Test exception',), {}):
            print(mid, res, data)

        async def sleeper():
            async for mid, res, data in conn.minion('glob', '*', 'test.sleep', (20,), {}):
                print(mid, res, data)

        task = asyncio.create_task(sleeper())
        await asyncio.sleep(1)
        task.cancel()

        async for mid, res, data in conn.minion('glob', '*', 'test.sleep', (20,), {}):
            print(mid, res, data)


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
