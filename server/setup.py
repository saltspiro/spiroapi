#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

setup(
    name='spiroapi_server',
    version='0.0.1',
    author="Jamie Bliss",
    author_email='jamie@ivyleav.es',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="An async-first RPC system for Salt",
    install_requires=[
        # From salt, but don't actually include salt directly
        'msgpack>=0.5,!=0.5.5',
        "tornado>=4.2.1,<6.0; python_version < '3'",
        "tornado>=4.2.1,<5.0; python_version >= '3.4'",
        "enum34; python_version < '3.4'",
    ],
    entry_points = {
        'console_scripts': [
            'spiroapi-ssh-subsystem=spiroapi_server.server:main'
        ],
    },
    long_description=readme,
    include_package_data=True,
    keywords='salt saltstack spiroapi salt-extension',
    packages=find_packages(include=['spiroapi_server']),
    setup_requires=[],
    test_suite='tests',
    tests_require=[],
    url='https://spirostack.com/spiroapi/',
    zip_safe=True,
)
