from __future__ import print_function, unicode_literals, division, absolute_import

import tornado.iostream
from tornado.ioloop import IOLoop
import msgpack
import logging

from .rawio import make_stdio_raw


class MsgPackProcServer(object):
    def __init__(self, recv_message):
        """
        * recv_message: Callback for when a message is received, in the form:
            recv_message(server, message)
        """
        self._recv_message = recv_message
        self._input, self._output, self._text = make_stdio_raw()
        self._packer = msgpack.Packer(autoreset=True)
        self._unpacker = msgpack.Unpacker(raw=False)

        self._buffer = bytes()

    def listen(self):
        self._input.set_close_callback(self._on_close)
        self._output.set_close_callback(self._on_close)

        self._input.read_until_close(streaming_callback=self._on_input)
        self._text.read_until_close(streaming_callback=self._on_text)

    def _on_input(self, data):
        """
        Data from the client.
        """
        self._unpacker.feed(data)
        for message in self._unpacker:
            IOLoop.current().spawn_callback(self._recv_message, self, message)

    def _on_text(self, data):
        """
        Data on stderr
        """
        # Not allowed to produce data on stderr
        self._buffer += data
        while b'\n' in self._buffer:
            data, self._buffer = self._buffer.split(b'\n', 1)
            data = data.decode('utf-8', errors='replace')
            try:
                self.send_message(data)
            except tornado.iostream.StreamClosedError:
                logging.getLogger().debug(data)

    def _on_close(self):
        IOLoop.current().stop()

    def send_message(self, message):
        # Not allowed to produce data on stderr
        data = self._packer.pack(message)
        self._output.write(data)

    def close(self):
        self._input.close()
        self._output.close()
