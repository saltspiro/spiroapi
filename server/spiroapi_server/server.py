from __future__ import print_function, unicode_literals, division, absolute_import

import enum
import logging
logging.basicConfig(level=logging.DEBUG, filename='/tmp/api.log')

from tornado.ioloop import IOLoop

import salt.client
import salt.runner
import salt.config
import salt.utils

from .ssh_msgpack_tornado import MsgPackProcServer
from .subman import SubscriptionManager

log = logging.getLogger(__name__)


class MsgID(enum.IntEnum):
    ShooshRequest = 0  # (C2S): request id

    StartMinionJob = 10  # (C2S): request id, target, target_type, function, pargs, kwargs
    MinionJobStarted = 11  # (S2C): request id, jid, speculated minions
    MinionJobResult = 12  # (S2C): request id, minion, return code, data
    MinionJobError = 14  # (S2C): request id, minion, message
    
    StartRunnerJob = 20  # (C2S): request id, function, pargs, kwargs
    RunnerJobStarted = 21  # (S2C): request id, jid
    RunnerJobComplete = 22  # (S2C): request id, data
    
    StartWheelJob = 30  # (C2S): request id, function, pargs, kwargs
    WheelJobStarted = 31  # (S2C): request id, jid
    WheelJobComplete = 32  # (S2C): request id, data
    
    EventSubscribe = 40  # (C2S): request id, pattern
    EventFired = 41  # (S2C): request id, tag, data


class ConnectionHandler(object):
    def __init__(self):
        self.loop = IOLoop.current()
        mopts = salt.config.client_config('/etc/salt/master')
        self.minion_client = salt.client.get_local_client(
            mopts=mopts,
            io_loop=self.loop,
        )
        self.runner_client = salt.runner.RunnerClient(mopts)
        self.salt_event = salt.utils.event.get_event(
            'master',
            opts=mopts,
            io_loop=self.loop,
            keep_loop=False,
            raise_errors=True,
        )
        self.salt_event.set_event_handler(self._recv_event)

        self.subs = SubscriptionManager()

    def _recv_event(self, raw):
        tag, data = self.salt_event.unpack(raw, self.salt_event.serial)
        log.debug("Event: %r %r", tag, data)
        self.subs.fire(tag, data)

    def __call__(self, server, message):
        """
        Entry point for messages.
        """
        if isinstance(message, list):
            idnum = message[0]
            args = message[1:]
            msgid = MsgID(idnum)

            method = getattr(self, 'do_' + msgid._name_)
            log.debug("Trying to call %r for %r", method, msgid)
            if method:
                method(server, *args)

    def do_ShooshRequest(self, server, reqid):
        log.debug("Shooshing %s", reqid)
        self.subs.unsub_group(reqid)

    def do_EventSubscribe(self, server, reqid, glob):
        print("Subscribing", glob)

        def _recv(tag, data):
            server.send_message(
                [MsgID.EventFired, reqid, tag, data]
            )

        self.subs.sub(glob, _recv, group=reqid)


    def do_StartMinionJob(self, server, reqid, target, target_type, func, pargs, kwargs):
        print("Starting", func)
        # FIXME: Use run_job_async() instead
        pubdata = self.minion_client.run_job(
            tgt=target,
            tgt_type=target_type,
            fun=func,
            arg=pargs,
            kwarg=kwargs,
            # ret
            # timeout
            # io_loop=self.loop,
        )
        server.send_message(
            [MsgID.MinionJobStarted, reqid, pubdata['jid'], pubdata['minions']]
        )
        tag = salt.utils.event.tagify(pubdata['jid'], 'job')

        def _new(tag, data):
            pass

        def _ret(tag, data):
            minion = data['id']
            if data['success']:
                server.send_message(
                    [MsgID.MinionJobResult, reqid, minion, data['retcode'], data['return']]
                )
            else:
                server.send_message(
                    [MsgID.MinionJobError, reqid, minion, data['return']]
                )

        self.subs.multisub(tag, {
            'new': _new,
            'ret/*': _ret,
        }, group=reqid)


def main():
    handler = ConnectionHandler()
    server = MsgPackProcServer(handler)
    print("running")
    server.listen()
    IOLoop.instance().start()
