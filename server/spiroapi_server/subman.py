from __future__ import print_function, unicode_literals, division, absolute_import

import fnmatch
import re
import logging

log = logging.getLogger(__name__)


class SubscriptionManager(object):
    def __init__(self):
        self._subscribers = list()

    def fire(self, tag, data):
        log.debug("Got event %s", tag)
        for func, group in self._find_funcs(tag):
            try:
                log.debug("Calling %r", func)
                unsub = func(tag, data)
            except Exception:
                log.exception("Error calling handler for %s", tag)
            else:
                if unsub is True:
                    log.debug("%r asked for unsub of %r", func, group)
                    self.unsub_group(group)

    def _find_funcs(self, tag):
        for pattern, func, group in self._subscribers:
            log.debug("Checking %r %r", pattern, func)
            if pattern.match(tag):
                yield func, group

    def _sub(self, tag, func, group):
        regex = fnmatch.translate(tag)
        self._subscribers.append((re.compile(regex), func, group))

    def _unsub(self, func):
        log.debug("Subscribers before: %i", len(self._subscribers))
        for p, f, g in self._subscribers[:]:
            log.debug("\t%s %s %s %r", p, f, g, func(f, g))
            if func(f, g):
                self._subscribers.remove((p, f, g))
        log.debug("Subscribers after: %i", len(self._subscribers))

    def sub(self, tag, func, group=None):
        self._sub(tag, func, group if group is not None else func)

    def unsub_func(self, func):
        self._unsub(lambda f, g: f == func)

    def unsub_group(self, group):
        log.debug("Unsubbing group %r", group)
        self._unsub(lambda f, g: g == group)

    def multisub(self, prefix, handlers, group=None):
        log.debug("Multisub g=%s %r", group, list(handlers.keys()))
        for suffix, func in handlers.items():
            self._sub(
                "{}/{}".format(prefix, suffix),
                func,
                group if group is not None else func
            )
