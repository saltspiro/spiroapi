# -*- coding: utf-8 -*-

"""Top-level package for SpiroAPI Client."""

__author__ = """Jamie Bliss"""
__email__ = 'jamie@ivyleav.es'
__version__ = '0.0.1'
