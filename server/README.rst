===============
SpiroAPI Server
===============


.. image:: https://img.shields.io/pypi/v/spiroapi_server.svg
        :target: https://pypi.python.org/pypi/spiroapi_server

.. image:: https://img.shields.io/gitlab/pipeline/spirostack/spiroapi.svg
        :target: https://gitlab.com/spirostack/spiroapi/pipelines


An async-first RPC system for Salt


* Free software: BSD license
* Documentation: https://spiroapi-client.readthedocs.io.


Features
--------

* TODO
